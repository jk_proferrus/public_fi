#!/bin/bash
mkdir -p -v $FPGA_INSIGHT_DIR/UPLOAD_QUEUE/
docker run -d --restart always  -p 8001:8001 --env-file $FPGA_INSIGHT_DIR/cfg/fpga_insight.lic   -v $FPGA_INSIGHT_DIR/UPLOAD_QUEUE/:/UPLOAD_QUEUE fpga_insight:latest fpga_insight upload_server