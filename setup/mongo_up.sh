#!/bin/bash
mkdir -p -v $FPGA_INSIGHT_DIR/MONGO_DATA/
docker run -d --restart always  -p 27017-27019:27017-27019  --env-file $FPGA_INSIGHT_DIR/cfg/mongo.cfg  -v $FPGA_INSIGHT_DIR/MONGO_DATA/:/data/db/ mongo:5.0.1  