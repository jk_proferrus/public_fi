# public_fi
HOW TO SETUP:
-------------

1. permanently set environment variable: 
    export FPGA_INSIGHT_DIR=<location> 
	
2. Create fpga_insight directory on the TARGET DRIVE WHERE BULD DATA WILL BE STORED:
    mkdir -p $FPGA_INSIGHT_DIR

3. download & untar common/public setup directory:
	 cd $FPGA_INSIGHT_DIR
	 wget https://gitlab.com/jk_proferrus/public_fi/-/archive/main/public_fi-main.tar.gz
	 tar --extract  --strip-components=1 --file=./public_fi-main.tar.gz   public_fi-main/setup/
	 rm -rf public_fi-main.tar.gz
	 
4. Create the config directory & customize as required (change port mappings and such): 
     cp -rf $FPGA_INSIGHT_DIR/setup/ref_cfg $FPGA_INSIGHT_DIR/cfg

5. Copy the fpga_insight.lic file into $FPGA_INSIGHT_DIR/cfg

6. Startup the mongodb server (use mongo_up_wsl.sh if running in windows-linux-subsystem since mongodb has issues with windows-bind-mounting)
     source $FPGA_INSIGHT_DIR/setup/mongo_up.sh 

7. Startup fi_submit_server (save to etl_queue)
8. startup fi_etl_server    (process files, upload info to DB, move to cache directory)
9. startup fi_webui_server  (user if)


